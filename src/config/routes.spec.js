'use strict';


import supertest from 'co-supertest'
import { expect } from 'chai'
import should from 'should'
import Db from 'db'
import _ from 'lodash'
import server from '../server'
import koa from 'koa'
import Debug from 'debug'
const debug = Debug('server')

let app = koa()

server(app)

/**
 * Koa config
 */

'use strict';

var config = require('./environment');
var morgan = require('koa-morgan');
import bodyparser from 'koa-bodyparser'
import router from 'koa-router'
import session from 'koa-session'
import PassportConfig from './PassportConfig'
PassportConfig()
import passport from 'koa-passport'
import passportHttp from 'passport-http'
import co from 'co'
import Db from 'db'
import _ from 'lodash'
import validator from 'koa-validator'


export default function(app) {
  let db = Db()
  let User = db.models.User
  app.use(router(app))
  app.use(morgan.middleware(config.logType));

  app.use(function *(next) {
    try {
      yield next;
    } catch (err) {
      if(err.name=='ValidationError'){
        this.status = 400
        this.body = err
        return
      }
      console.log(err);
      this.status = err.status || 500;
      this.body = err.message;
      this.app.emit('error', err, this);
    }
  })

  //keys
  app.keys=['support']
  
  app.use(session(app))

  app.use(bodyparser())
  // Logger 
  //Passport
  
  passport.use(new passportHttp.BasicStrategy(function(username, password, done) {
    co(function*(){
      username = yield User
      .findById(username)
      .exec()

      if(username.verifyPasswordSync(password)){
        return username.toJSON()
      }
      return false
    })
    .then(function(user){
      done(null, user)
    })
    .catch(function(err){
      done(err)
    })
  }))
  
  passport.serializeUser(function(user, done){
    done(null, user)
  })
  passport.deserializeUser(function(user, done){
    done(null, user)
  })

  app.use(passport.initialize());

  app.use(passport.session({secret:'test'}));
  app.use(validator({
    onValidationError: function*(errMsg){
      console.log(errMsg);
    }
  }))  
};
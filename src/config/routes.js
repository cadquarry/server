/**
 * Main application routes
 */

'use strict';

import mount from 'koa-mount'
import route from 'koa-route'
import generateApi from 'koa-mongo-rest'
import Db from 'db'
import _ from 'lodash'
import Debug from 'debug'
const debug = Debug('server')
const db = new Db()

export default function(app, baseUrl) {
  baseUrl = baseUrl||''
  app.db = db
  _.forOwn(db.models, model=>{
    generateApi(app, model, baseUrl)
  })
  //already has auth built in
  // app.use(mount(`${baseUrl}/user`, require('../resources/user')))
}

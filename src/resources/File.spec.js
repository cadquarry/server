'use strict';


import supertest from 'co-supertest'
import { expect } from 'chai'
import should from 'should'
import Db from 'db'
import _ from 'lodash'
import server from '../server'
import koa from 'koa'
import Debug from 'debug'
import Storage from 'storage'
import agent from 'superagent'
import ms from 'ms'

const storage = new Storage('cadq')
const imgStorage = new Storage('cadimg')

let imgUrl = 'https://s3.amazonaws.com/cadimg/55836ab2c6e096f4d5659a6d.jpg'
let fileUrl = 'https://s3.amazonaws.com/cadq/55836874b9421569d5675472.zip'
const debug = Debug('server')

let app = koa()
server(app)

let request = supertest.agent(app.listen())

let Model = app.db.models.File

function* Data(){
  let name = _.uniqueId('test_file_')
  let file = new Model({
    title: `${name} title`
  })

  let imgName = `${file._id}.jpg`
  let img = agent.get(imgUrl)
  let header = {
    'Content-Type':'image/jpeg'
  }
  // debug(imgName)
  yield imgStorage.putStreamBuffer(img, imgName, header)

  let filename = `${file._id}.zip`
  let _file = agent.get(fileUrl)
  header = {
    'Content-Type':'application/zip'
  }
  yield storage.putStreamBuffer(_file, filename, header)

  file.set('filename', filename)
  file.set('img', imgName)
  return file
}

describe('File', function(){
  this.timeout(ms('1m'))
  beforeEach(function*(){
    this.data = yield Data()
  })
  afterEach(function*(){
    yield this.data.remove()
  })
  it('should create a file', function* () {
    let data = this.data.toJSON()
    let res = yield request
    .post(`/files`)
    .send(this.data.toJSON())
    .expect(201)
    .end()

    yield Model.findByIdAndRemove(res.body._id)
  });
  it.only('401 POST /files', function* () {
    let data = this.data.toJSON()
    let res = yield request
    .post(`/files`)
    .send(this.data.toJSON())
    .expect(401)
    .end()
    
    // yield Model.findByIdAndRemove(res.body._id)
  });
})

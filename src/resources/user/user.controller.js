'use strict';
import Db from 'db'
import _ from 'lodash'
let db
let Schema
let SchemaName = 'User'
let PopulateKeys = []

class Controller {
  constructor(){
    db = Db()
    Schema = db.models[SchemaName]
  }
  *get(next) {
    if(!this.params.id){
      this.body = this.session.passport.user
      this.status = 200
      return;
    }
    this.body = yield Schema
    .findById(this.params.id)
    .select('-__v')
    // .populate(PopulateKeys)
    .exec()
    if(!this.body){
      this.body = {message: 'not found.'}
      return this.status = 404
    }
    this.body = _.pick(this.body, '_id')
  	this.status = 200
  }
  *post(next){
    let p = new Schema(this.request.body)
    yield p.save()

    this.status = 200
    this.body = p.toJSON()
  }
  *put(next){
    let ctx = this
    let data = yield Schema
    .findById(this.params.id)
    .exec()

    let keys = _.keys(this.request.body)
    keys.forEach(k=>{
      data.set(k, ctx.request.body[k])
    })
    yield data.save()

    this.body = yield Schema
    .findById(this.params.id)
    .exec()

    this.status = 200
  }
  *delete(next){
    yield Schema
    .findByIdAndRemove(this.params.id)
    .exec()

    this.status = 200
    this.body = {}
  }
}

export default Controller
'use strict';
import Controller from './user.controller'
import auth from '../../middleware/BasicAuth'
let ctrlr = new Controller()
var router = require('koa-router')();

router.post('/logout', auth);
router.post('/', ctrlr.post);
router.put('/:id', auth, ctrlr.put);
router.delete('/:id', auth, ctrlr.delete);
router.get('/', auth, ctrlr.get);
router.get('/:id', ctrlr.get);
module.exports = router.routes();
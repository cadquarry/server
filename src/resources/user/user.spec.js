'use strict';


import supertest from 'co-supertest'
import { expect } from 'chai'
import should from 'should'
import Db from 'db'
import _ from 'lodash'
import server from '../../server'
import koa from 'koa'

let app = koa()

server(app)

var request = supertest.agent(app.listen())

let db
let SchemaName = 'User'
let resourceUrl = '/user'
let Keys = [
  '_id'
  , 'password'
  , 'FName'
  , 'LName'
]
function *remove(obj){
  yield db.models[SchemaName]
  .findOneAndRemove(_.pick(obj, ['_id']))
  .exec()
}

describe(`${resourceUrl}`, function(){

  before(function*(){
    db = Db()
  })

  after(function*(){
    db.close()
  })
  function rndData(){
    return new db.models[SchemaName]({
      _id: rndEmail()
      , password: _.uniqueId('password_')
      , FName: _.uniqueId('FName_')
      , LName: _.uniqueId('LName_')
    })
  }
  function rndEmail(){
    let rnd = _.random(1000, 9999)
    rnd = `${rnd}@test.com`
    return rnd
  }
  beforeEach(function*(){
    
    this.password = _.uniqueId('password_')
    this.data = new db.models[SchemaName]({
      _id: rndEmail()
      , password: this.password
      , FName: _.uniqueId('FName_')
      , LName: _.uniqueId('LName_')
    })

    yield this.data.save()
  })

  afterEach(function*(){
    yield this.data.remove()
  })

  it('POST / 200', function*(){
    let data = _.omit(rndData().toJSON(), ['__v'])
    let res = yield request
    .post(`${resourceUrl}`)
    .send(data)
    .expect(200)
    .end()
    yield remove(res.body)
  })
  it('POST / 400', function*(){
    let data = _.omit(rndData().toJSON(), ['__v'])
    delete data.password
    let res = yield request
    .post(`${resourceUrl}`)
    .send(data)
    .expect(400)
    .end()
    yield remove(data)
    expect(res.body.name).to.be.equal('ValidationError')
  })
  it('PUT /:id 401', function*(){
    let data = _.omit(this.data.toJSON(), ['__v'])
    let res = yield request
    .put(`${resourceUrl}/${data._id}`)
    .send(data)
    .expect(401)
    .end()
  })
  it('PUT /:id 200', function*(){
    let data = _.omit(this.data.toJSON(), ['__v'])
    let oldPwd = data.password
    data.password = _.uniqueId('#password_')
    let res = yield request
    .put(`${resourceUrl}/${data._id}`)
    .auth(this.data._id, this.password)
    .send(data)
    .expect(200)
    .end()
    expect(oldPwd).to.not.be.deep.equal(res.body.password)
  })
  it('GET /:id 200', function*(){
    let res = yield request
    .get(`${resourceUrl}/${this.data._id}`)
    .auth(this.data._id, this.password)
    .expect(200)
    .end() 
    expect(res.body._id).to.be.deep.equal(this.data._id.toString())
  })
  it('DEL /:id 200', function*(){
    let res = yield request
    .del(`${resourceUrl}/${this.data._id}`)
    .auth(this.data._id, this.password)
    .expect(200)
    .end()
  })
  it('POST /logout 200', function*(){
    let res = yield request
    .post(`${resourceUrl}/logout`)
    // .auth(this.data._id, this.password)
    .expect(200)
    .end()
  })
  it('PUT /:id 200', function*(){
    let data = _.omit(this.data.toJSON(), ['__v'])
    let oldPwd = data.password
    data.password = _.uniqueId('#password_')
    let res = yield request
    .put(`${resourceUrl}/${data._id}`)
    .send(data)
    .expect(401)
    .end()
  })
  it('GET / 200', function*(){
    yield request
    .post(`${resourceUrl}/logout`)
    .expect(200)
    .end()

    let res = yield request
    .get(`${resourceUrl}`)
    .auth(this.data._id, this.password)
    .expect(200)
    .end()
    expect(res.body._id).to.be.deep.equal(this.data._id.toString())
  })
})
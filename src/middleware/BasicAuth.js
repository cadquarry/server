import passport from 'koa-passport'
import _ from 'lodash'

export default function *auth(next){
  let ctx = this
  if(this.url=='/logout'){
    this.session.passport = {}
    this.status = 200
    this.body = {}
    return yield next
  }
    if(this.session.passport.user)return yield next;
    yield passport.authenticate('basic', {session:true},function*(err, user, info){
      if(err) throw err
      if(!user){
        ctx.status = 401
        ctx.body = {message: 'Unauthorized access.'}
        return
      }
      yield ctx.login(user)
      yield next
    })
  }